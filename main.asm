%include "dict.inc"
%include "lib.inc"
%include "words.inc"

%define BUF_SIZE 255
%define SYSTEM_WRITE 1
%define STDERR 2
%define EXIT_CODE_ERROR 52

section .bss
BUF:
resb (BUF_SIZE + 1)

section .rodata
err_msg1:
db "line reading error", 10, 0
err_msg2:
db "no entry found error", 10, 0

section .text
global _start

_start:
	mov	rdi, BUF
	mov	rsi, BUF_SIZE
	call	read_word
	test	rax, rax
	jnz	.next
.error_reeading:
	mov	rdi, err_msg1
	call	print_error_message
	mov	rdi, EXIT_CODE_ERROR
	call	exit
.error_not_found:
	mov	rdi, err_msg2
	call	print_error_message
	mov	rdi, EXIT_CODE_ERROR
	call	exit
.next:
	mov	rsi, DICT
	mov	rdi, BUF
	call	find_word
	test	rax, rax
	jz	.error_not_found
	mov	rdi, rax
	add	rdi, 8
	push	rdi
	call	string_length
	pop	rdi
	add	rdi, rax
	inc	rdi
	call	print_string
	xor	rdi, rdi
	call	exit
	
print_error_message:
	push	rdi
	call	string_length
	pop	rsi
	mov	rdx, rax
	mov	rax, SYSTEM_WRITE
	mov	rdi, STDERR
	syscall
	ret
	
