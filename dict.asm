; Файл с фукнцией find_word, которая принимает два аргумента:
; * Указатель на нуль-термированную строку
; * Указатель на начало словаря
; find_word пройдёт по всему словарю в поисках подходящего ключа.
;  Если подходящее вхождение найдено, вернёт адрес начала 
; вхождения в   словарь (не значения), иначе вернёт 0.

%include "lib.inc"
global find_word

;rdi - Указатель на нуль-термированную строку
;rsi - Указатель на начало словаря
find_word:
	push	r12
.loop:
	mov	r12, rsi
	lea 	rsi, [rsi+8]
	call	string_equals
	test	rax, rax
	jz	.next
	mov	rax, r12
	pop	r12
	ret
.next:
	mov	rsi, [r12]
	test	rsi, rsi
	jnz	.loop
	xor	rax, rax
	pop	r12
	ret
	
