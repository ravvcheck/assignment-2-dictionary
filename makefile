LD=ld
ASM=nasm
ASMFLAGS=-f elf64
PROGRAM=main
SRC=main.asm lib.asm dict.asm
OBJ=$(SRC:%.asm=%.o)

main.o: main.asm lib.inc words.inc dict.inc
dict.o: dict.asm lib.inc
words.inc: colon.inc

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

$(PROGRAM): $(OBJ) 
	$(LD) -o $@ $+ 

.PHONY: clean test

test.py: $(PROGRAM)
	python3 test.py

clean:
	rm -f $(PROGRAM) $(OBJ)
